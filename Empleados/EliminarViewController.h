//
//  EliminarViewController.h
//  Empleados
//
//  Created by Yeffers23 on 5/6/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Empleados.h"
@interface EliminarViewController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *txtCedula;
@property (strong, nonatomic) IBOutlet UILabel *labelStatus;


- (IBAction)EliminarButton:(id)sender;
 
@end
