//
//  ActualizarViewController.h
//  Empleados
//
//  Created by Yeffers23 on 5/6/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActualizarViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *cedulatxt;

@property (strong, nonatomic) IBOutlet UITextField *nombreTxt;
@property (strong, nonatomic) IBOutlet UITextField *direccionTxt;
@property (strong, nonatomic) IBOutlet UITextField *edadTxt;
@property (strong, nonatomic) IBOutlet UILabel *labelStatus;


- (IBAction)buscarbutton:(id)sender;
- (IBAction)actualizarbutton:(id)sender;

@end
