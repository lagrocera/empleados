//
//  EliminarViewController.m
//  Empleados
//
//  Created by Yeffers23 on 5/6/15.
//  Copyright (c) 2015 UdeM. All rights reserved.
//

#import "EliminarViewController.h"
#import "Empleados.h"
@interface EliminarViewController (){
    Empleados *eliminarEmpleado;
}

@end

@implementation EliminarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    eliminarEmpleado =[[Empleados alloc]init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)EliminarButton:(id)sender {
    eliminarEmpleado.empCedula = _txtCedula.text;
    [eliminarEmpleado deleteEmployedFromDatabase];
    
    _labelStatus.text=eliminarEmpleado.status;
    _txtCedula.text= @"";
    
}
@end
